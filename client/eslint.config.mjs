import globals from "globals";
import pluginJs from "@eslint/js";
import pluginReactConfig from "eslint-plugin-react/configs/recommended.js";


export default [
  {languageOptions: { globals: globals.browser }},
  pluginJs.configs.recommended,
  pluginReactConfig,
  {
    ignores: [
      "scripts/*",
      "config/*",
    ],
  },
  {
    rules: {
      "no-undef": "warn",
      "react/react-in-jsx-scope": "warn",
      "react/prop-types": "warn",
      "react/no-unescaped-entities": "warn",
      "no-unused-vars": "warn"
    }
  }
];